#include <stdio.h>
#include <iostream>
#include "smartpointer.h"
using namespace std;
int RC :: Zwroc()
    {
        return ref;
    }
void RC ::  AddRef()
{

        ref+=1;

}
template < typename T >
SmartPointer<T>::SmartPointer() : ptr(0), reference(0)
    {
        reference = new RC();
        reference->AddRef();
    }
template < typename T >
SmartPointer<T>::SmartPointer(T* pValue) : ptr(pValue), reference(0)
    {
        reference = new RC();
        reference->AddRef();
    }
template < typename T >
SmartPointer<T>::SmartPointer(const SmartPointer<T>& sp) : ptr(sp.ptr), reference(sp.reference)
    {
        reference->AddRef();
    }
template < typename T >
SmartPointer<T>::~SmartPointer()
    {
        if(reference->Zwroc() == 0)
        {
            delete ptr;
            delete reference;
        }
    }
template < typename T >
T& SmartPointer<T>:: operator* ()
    {
        return *ptr;
    }
template < typename T >
T& SmartPointer<T>:: operator-> ()
    {
        return ptr;
    }
template < typename T >
SmartPointer<T>& SmartPointer<T>:: operator = (const SmartPointer<T>& sp)
    {
        if (this != &sp)
        {
            ptr = sp.ptr;
            reference = sp.reference;
            reference->AddRef();
        }
        return *this;
    }
 template < typename T >
void SmartPointer<T>:: wypisz()
    {
    cout<<reference->Zwroc()<<endl;
    }
