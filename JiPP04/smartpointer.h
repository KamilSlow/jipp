#ifndef _SmartPointer_H_
#define _SmartPointer_H_
class RC
{
private:
    int ref;
public:
    int Zwroc();
   void AddRef();
};
template < typename T >
class SmartPointer
{
private:
    T*    ptr;
    RC* reference;
public:
    SmartPointer();

    SmartPointer(T* );

    SmartPointer(const SmartPointer<T>& );

    ~SmartPointer();

    T& operator* ();

    T& operator-> ();

    SmartPointer<T>& operator = (const SmartPointer<T>& );
    void wypisz();
};
#endif _SmartPointer_H_
