#ifndef LISTA
#define LISTA

#include <iostream>

template <typename T>
class List
{
	private:
	class ogniwo
	{
	    public:
		T value;
		ogniwo * prev;
		ogniwo * next;
	};
    ogniwo * first;
	ogniwo * last;
	int lsize;
	public:
    List();
    void push_back (T);
	void push_front (T);
	T pop_front();
	T pop_back();
	bool empty();
	int size();
	T front();
	T back();
	void insert(int,T);
};

#endif
