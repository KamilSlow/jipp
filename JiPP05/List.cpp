#include "List.h"
using namespace std;

template <typename T>
List<T>::List()
{
	lsize=0;
	first=0;
	last=0;
}

template <typename T>
void List<T>::push_back(T w)
{
	ogniwo *temp=new ogniwo;
    temp->value=w;
	if (first==0)
	{

		temp->prev=0;
		temp->next=0;
        first=temp;
		last=temp;

	}
	else
	{
		temp->prev=last;
		temp->next=0;
		last=temp;
	}
	lsize++;
}

template <typename T>
void List<T>::push_front(T w)
{
	ogniwo *temp=new ogniwo;
    temp->value=w;
	if (first==0)
	{

		temp->prev=0;
		temp->next=0;
        first=temp;
		last=temp;
	}
	else
	{
		temp->next=first;
		temp->prev=0;
		first=temp;
	}
	lsize++;
}
template <typename T>
T List<T>::pop_front()
{
    if (lsize==0)
	{
		cout<<"Lista jest pusta!\t";
		return 0;
	}
	lsize--;
	ogniwo *temp=new ogniwo;
	temp=first;
	first=temp->next;
	T t = temp->value;
	delete temp;
	return t;
}

template <typename T>
T List<T>::pop_back()
{
    if (lsize==0)
	{
		cout<<"Lista jest pusta!\t";
		return 0;
	}
	lsize--;
	ogniwo *temp=new ogniwo;
	temp=last;
    last=temp->prev;
	T t = temp->value;
	delete temp;
	return t;
}

template <typename T>
T  List<T>::back()
{
	if (lsize==0)
	{
		cout<<"Lista jest pusta!\t";
		return 0;
	}
	return last->value;
}

template <typename T>
T List<T>::front()
{
	if (lsize==0)
	{
		cout<<"Lista jest pusta!\t";
		return 0;
	}
	return first->value;
}

template <typename T>
void List<T>::insert(int num, T w)
{
	if (lsize==0)
	{
		cout<<"Lista jest pusta!\t";
	}

	else
	{
    if (num>lsize || num<1)
	{
		cout<<"Insert poza zasiegiem\n";
	}
	else
	{
		ogniwo *nowy = new ogniwo;
		ogniwo *temp;
		temp=first;
        for(int i=1;i<num;i++)
		{
			temp=temp->next;
		}
		nowy->prev=temp->prev;
		nowy->next=temp;
		temp->prev=nowy;
		nowy->value=w;
		if(num==1)
        {
        first=nowy;
        }
	}
	}
}
template <typename T>
int List<T>::size()
{
	return lsize;
}

template <typename T>
bool List<T>::empty()
{
	if(lsize==0)return 1;
	else return 0;
}
