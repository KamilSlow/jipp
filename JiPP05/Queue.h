#ifndef QUEUE
#define QUEUE

#include <iostream>

template <typename T>
class Queue
{
    int maxsize;
	T *q;
	int qsize;
	public:
    Queue();
    ~Queue();
    void push_back (T );
    T pop_front();
    T front();
	T back();
	bool empty();
	int size();

};

#endif
