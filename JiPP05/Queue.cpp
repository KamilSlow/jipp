#include "Queue.h"
using namespace std;
template <typename T>
Queue<T>::Queue()
{
	qsize=0;
	maxsize=20;
	q=new T[maxsize];
}
template <typename T>
Queue<T>::~Queue()
{
    delete q;
}
template <typename T>
void Queue<T>::push_back(T nowy)
{
	if(qsize<maxsize)
	{
    q[qsize]=nowy;
	qsize++;
    }
    else
    {
		cout<<"Kolejka jest pelna!\n";
    }
}

template <typename T>
T Queue<T>::pop_front()
{	if(qsize==0)
	{
		cout<<"Kolejka jest pusta!\n";
		return 0;
	}
	int i=0;
	T p=q[0];
	qsize--;
    for(i;i<qsize;i++)
	{
		q[i]=q[i+1];
	}
	return p;
}

template <typename T>
T Queue<T>::front()
{
	if(qsize==0)
	{
		cout<<"Kolejka jest pusta!\t";
		return 0;
	}
	return q[0];
}

template <typename T>
T  Queue<T>::back()
{
	if(qsize==0)
	{
		cout<<"Kolejka jest pusta!\t";
		return 0;
	}
	return q[qsize-1];
}

template <typename T>
bool Queue<T>::empty()
{
	if(qsize==0)return 1;
	else return 0;
}

template <typename T>
int Queue<T>::size()
{
	return qsize;
}
