#ifndef _Settings_H_
#define _Settings_H_
class Settings {
	private:
		char * connectionString;
		char * sciezka;
        Settings(const Settings &){};
        Settings();
        ~Settings();
	public:
		static int counter;
		void UstawPolaczenieZBazaDanych(char *);
		void UstawPlikKonfiguracyjny(char *);
		static Settings & getInstance();

};
#endif _Settings_H_
