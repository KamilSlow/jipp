
#include<math.h>
#include"Point2D.h"

using namespace std;

Point2D::Point2D(): x(0), y(0){}
Point2D::Point2D(float l1, float l2): x(l1), y(l2){}
Point2D::Point2D(float l0): x(l0), y(0){}
Point2D::operator double()
{
    return sqrt(x*x+y*y);
}

const Point2D Point2D::operator+ (const Point2D & p) const
{
return Point2D(x + p.x, y + p.y);
}

const Point2D Point2D::operator- (const Point2D & p) const
{
return Point2D(x - p.x, y - p.y);
}

const Point2D Point2D::operator/ (const float & d)const
{
return Point2D(x / d, y / d);
}

const Point2D Point2D::operator* (const float & d)const
{
return Point2D(x * d, y * d);
}

const Point2D& Point2D:: operator+= (const Point2D & p)
{
this->x += p.x;
this->y += p.y;
return *this;
}

const Point2D& Point2D:: operator-= (const Point2D & p)
{
this->x -= p.x;
this->y -= p.y;
return *this;
}

const Point2D& Point2D:: operator/= (const float & d)
{
this->x /= d;
this->y /= d;
return *this;
}

const Point2D& Point2D:: operator*= (const float & d)
{
this->x *= d;
this->y *= d;
return *this;
}

bool Point2D:: operator== (const Point2D &p)const
{
if(x==p.x && y==p.y)
    return true;
else
    return false;
}

void test(bool g)
{
 if ( g == true )
    cout<<"PRAWDA\n";
  else
    cout<<"FALSZ\n";
}

ostream& operator<< (ostream &wyjscie, const Point2D & p)
{
   wyjscie << "("<<p.x<<","<<p.y<<")"<< endl;
   return wyjscie;
}

istream& operator>> (istream &wejscie, Point2D& p)
{
   wejscie >> p.x >> p.y;
   return wejscie;
}


