#ifndef _Point2D_H_
#define _Point2D_H_
using namespace std;
class Point2D
{
private:
    float x;
    float y;

public:
Point2D();
Point2D(float , float );
Point2D(float );
const Point2D operator+ (const Point2D &) const;
const Point2D operator- (const Point2D &) const;
const Point2D operator/ (const float &)const;
const Point2D operator* (const float &)const;
const Point2D& operator+= (const Point2D &);
const Point2D& operator-= (const Point2D &);
const Point2D& operator/= (const float &);
const Point2D& operator*= (const float &);
bool operator== (const Point2D &)const;
friend ostream& operator<< (ostream&,const Point2D &);
friend istream& operator>> (istream&,Point2D&);
operator double();
};
#endif _Point2D_H_
